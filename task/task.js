const colour = require('colour');

function run(task, action, ...args) {
  const command = process.argv[2];
  const taskName = command && !command.startsWith('-') ? `${task}:${command}` : task;
  const start = new Date();
  process.stdout.write(`Starting '${taskName}'...\n`.yellow);
  return Promise.resolve()
  .then(() => {
    return new Promise((res, rej) => {
      return action(res, rej, ...args)
    }).catch((error) => {
      console.log( errorLog(error) );
      new Error(error);
    });
  })
  .then(() => {
    process.stdout.write(`Finished '${taskName}' after ${new Date().getTime() - start.getTime()}ms\n`.green);
  }, err => process.stderr.write(`${err.stack}\n`));
}

process.nextTick(() => require.main.exports());
exports.run = (task, action) => run.bind(undefined, task, action);

const errorLog = (error) => {
  return `\nError: ${error}\n`;
};









