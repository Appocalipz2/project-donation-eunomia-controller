const fs = require(`fs`);
const ejs = require(`ejs`);
const system = require(`./../../variables`)(`./eunomia.json`);

const isMissing = (data, message) => typeof data === 'undefined'

exports.generate = (writeToPath) => require(`./../../task/index`)(`crossdomain`, (resolve, reject) => {
  if (!fs.existsSync(`./eunomia.json`)) {
    reject("eunomia.json required within the root of the project")
  }
  else {
    const eunomia = system;

    if (!eunomia.generate.crossdomain.enabled) {
      resolve()
      return false
    }

    if (isMissing(eunomia.generate.crossdomain)) {
      reject("The `eunomia.json` requires a `crossdomain` json` field")
    }
    else {
      try {
        const fullPath = `./node_modules/eunomia-controller/generate/crossdomain/crossdomain.ejs`
        const template = fs.readFileSync(fullPath, `utf8`);
        const render = ejs.compile(template, { filename: fullPath });
        const output = render({});
        fs.writeFileSync(`${writeToPath}/crossdomain.xml`, output, `utf8`);
        resolve()
      }
      catch (e) {
        console.log(e)
        reject("Failed to create a crossdomain.txt")
      }
    }
  }
});