const fs = require(`fs`);
const ejs = require(`ejs`);
const path = require('path');
const system = require(`./../../variables`)(`./eunomia.json`);

exports.generate = (writeToPath, srcImages) => require(`./../../task/index`)(`assets`, (resolve, reject) => {
  const subDirectories = (src, sub) => {
    var folders = fs.readdirSync(src).filter(file => fs.lstatSync(path.join(src, file)).isDirectory()).map(folder => ({
      src: src,
      folder: `/${folder}`,
      folderPath: `${sub}/${folder}`,
      fullPath: `${src}/${folder}`,
      files: fs.readdirSync(`${src}/${folder}/`).filter(file => !fs.lstatSync(path.join(`${src}/${folder}/`, file)).isDirectory()),
      target: `${writeToPath}assets/images${sub}/${folder}`
    }));
    var subFolders = [].concat.apply([], folders.map(item => subDirectories(item.fullPath, item.folderPath)));
    return folders.concat(subFolders);
  };

  const copyFile = (src, dest) => {
    let readStream = fs.createReadStream(src);
    readStream.once('error', (err) => {
      console.log(err);
    });
    readStream.once('end', () => {});
    readStream.pipe(fs.createWriteStream(dest));
  };


  if (!fs.existsSync(`./eunomia.json`)) {
    reject("eunomia.json required within the root of the project")
  }
  else {
    const eunomia = system;

    try {
      if (!fs.existsSync(`${writeToPath}assets`)){ fs.mkdirSync(`${writeToPath}assets`); }
      if (!fs.existsSync(`${writeToPath}assets/images`)) { fs.mkdirSync(`${writeToPath}assets/images`); }

      //TODO: Start from root
      subDirectories(`${srcImages}`, "").map(dir => {
        if (!fs.existsSync(`${writeToPath}assets/images${dir.folderPath}`)){
          fs.mkdirSync(`${writeToPath}assets/images${dir.folderPath}`);
        }
        dir.files.map(file => {
          copyFile(`${dir.fullPath}/${file}`, `${dir.target}/` + file)
        })
      });

      fs.readdirSync(`${srcImages}`).filter(file => !fs.lstatSync(path.join(`${srcImages}/`, file)).isDirectory()).map(file => {
        copyFile(`${srcImages}/` + file, `${writeToPath}assets/images/` + file)
      });

      resolve();
    }
    catch (e) {
      reject(e);
    }
  }
});

