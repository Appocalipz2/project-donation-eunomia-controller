const fs = require(`fs`);
const ejs = require(`ejs`);
const eunomia = require(`./../../variables`)(`./eunomia.json`);
const isUndefined = require("../../common").isUndefined

// Generate the HTML content for the browserConfig file
const generateHTML = (__ld) =>
  (isUndefined(__ld) ? "" : __ld.map(__d => {
    const body = (__bd) => (typeof __bd === 'string' ? __bd : generateHTML(__bd))
    return `<${__d.id}${(typeof __d.attributes === 'undefined' ? "" : ` ${__d.attributes.map(attr => `${attr[0]}="${attr[1]}"`).join(" ")}`)}>${body(__d.body)}</${__d.id}>`
  }).join(``))

exports.generate = (writeToPath) => {
  try {
    // Is this module enabled
    if (!eunomia.generate.browserconfig.enabled) {
      // Create a dummy promise that instant resolves to continue the flow
      return Promise.resolve()
        .then(() => {
          return new Promise((res, rej) => {
            return res()
          })
        })
    }
    else {
      // Retrieve the task function - it create a pretty promise with finish text and time
      return require(`./../../task/index`)(`browserconfig`, (resolve, reject) => {
        // Make sure the eunomia.json file exists
        if (!fs.existsSync(`./eunomia.json`)) {
          reject(`eunomia.json required within the root of the project`)
        }
        else {
          // Make sure the browserconfig json field exists - if not the browserconfig will not be generated
          if (isUndefined(eunomia.generate.browserconfig))
            resolve()
          else {
            try {
              // Full path to the ejs the browserconfig will use
              const fullPath = `./node_modules/eunomia-controller/generate/browserconfig/browserconfig.ejs`
              // Get the ejs content
              const template = fs.readFileSync(fullPath, `utf8`);
              // Render the json content into the ejs content
              const render = ejs.compile(template, { filename: fullPath });
              const output = render({content: generateHTML(eunomia.generate.browserconfig.body)});
              // Write the result of the combined json and ejs into the respective file
              fs.writeFileSync(`${writeToPath}/browserconfig.xml`, output, `utf8`);
              // Resolve the promise to continue the flow
              resolve()
            }
            catch (e) {
              reject(`Failed to create a browserconfig.xml`)
            }
          }
        }
      })
    }
  }
  // If for some reason something goes wrong - most common the chmod permission is incorrect it will fall in this catch
  catch (e) {
    console.log( "Error: Eunomia - Generating the `browserconfig` file" );
    return Promise.resolve()
      .then(() => {
        return new Promise((res, rej) => {
          return res()
        })
      }).catch((error) => {
        console.log( "Advanced Error: Eunomia - Generating the `browserconfig` file && try, catch Promise" );
        new Error(error);
      });
  }
};