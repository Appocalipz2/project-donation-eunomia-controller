const fs = require(`fs`);
const ejs = require(`ejs`);
const system = require(`./../../variables`)(`./eunomia.json`);

const isMissing = (data, message) => typeof data === 'undefined'

exports.generate = (writeToPath) => require(`./../../task/index`)(`humans`, (resolve, reject) => {
  if (!fs.existsSync(`./eunomia.json`)) {
    reject("eunomia.json required within the root of the project")
  }
  else {
    const eunomia = system;
    if (!eunomia.generate.humans.enabled) {
      resolve()
      return false
    }

    // Structure Test
    if (isMissing(eunomia.generate)) {
      reject("The `eunomia.json` requires a `generate` json field")
    }

    // Structure Humans Field Test
    else if (typeof eunomia.generate.humans === 'undefined') {
      reject("The `eunomia.json` requires a `generate.humans` json field")
    }
    else if (typeof eunomia.generate.humans !== 'object') {
      reject("The `generate.humans` in the `eunomia.json` needs to be a json object")
    }

    // Company field within the humans json
    else if (isMissing(eunomia.generate.humans.company)) {
      reject("The `eunomia.json` requires a `generate.humans.company` json field")
    }
    else if (typeof eunomia.generate.humans.company !== 'string') {
      reject("The `generate.humans.company` in the `eunomia.json` needs to be a string")
    }

    // Developers field within the humans json
    else if (isMissing(eunomia.generate.humans.developers)) {
      reject("The `eunomia.json` requires a `generate.humans.developers` json field")
    }
    else if (typeof eunomia.generate.humans.developers !== 'object') {
      reject("The `generate.humans.developers` in the `eunomia.json` needs to be a json object")
    }
    else if (eunomia.generate.humans.developers.filter(developer => typeof developer.title == 'undefined' || typeof developer.name == 'undefined' || typeof developer.age == 'undefined' || typeof developer.website == 'undefined').length > 0) {
      reject("The `generate.humans.developers` array may only contains json objects that yields: title, name, age & website")
    }

    // Company field within the humans json
    else if (isMissing(eunomia.generate.humans.technologies)) {
      reject("The `eunomia.json` requires a `generate.humans.technologies` json field")
    }
    else if (typeof eunomia.generate.humans.technologies !== 'object') {
      reject("The `generate.humans.technologies` in the `eunomia.json` needs to be a json object")
    }

    else {
      try {
        const fullPath = `./node_modules/eunomia-controller/generate/humans/humans.ejs`
        const template = fs.readFileSync(fullPath, `utf8`);
        const render = ejs.compile(template, {
          filename: fullPath
        });
        const output = render({eunomia});
        fs.writeFileSync(`${writeToPath}/humans.txt`, output, `utf8`);
        resolve()
      }
      catch (e) {
        console.log(e)
        reject("Failed to create a humans.txt")
      }
    }
  }
});