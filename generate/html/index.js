module.exports = (writeToPath, distToPath, stats, production = false) => require(`./html`).generate(writeToPath, distToPath, stats, production);
