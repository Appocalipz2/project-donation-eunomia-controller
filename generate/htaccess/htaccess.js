const fs = require(`fs`);
const ejs = require(`ejs`);
const eunomia = require(`./../../variables`)(`./eunomia.json`);

const isMissing = (data, message) => typeof data === 'undefined'

exports.generate = (writeToPath) => require(`./../../task/index`)(`htaccess`, (resolve, reject) => {
  if (!fs.existsSync(`./eunomia.json`)) {
    reject("eunomia.json required within the root of the project")
  }
  else {
    if (!eunomia.generate.htaccess.enabled) {
      resolve()
      return false
    }

    if (isMissing(eunomia.generate.htaccess)) {
      reject("The `eunomia.json` requires a `htaccess` json` field")
    }
    else {
      try {
        const fullPath = `./node_modules/eunomia-controller/generate/htaccess/htaccess.ejs`
        const template = fs.readFileSync(fullPath, `utf8`);
        const render = ejs.compile(template, { filename: fullPath });
        const output = render({});
        fs.writeFileSync(`${writeToPath}/.htaccess`, output, `utf8`);
        resolve()
      }
      catch (e) {
        console.log(e)
        reject("Failed to create a .htaccess")
      }
    }
  }
});