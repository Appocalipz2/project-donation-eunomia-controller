const fs = require(`fs`);
const ejs = require(`ejs`);
const system = require(`./../../variables`)(`./eunomia.json`);

const isMissing = (data, message) => typeof data === 'undefined'

exports.generate = (writeToPath) => require(`./../../task/index`)(`sitemap`, (resolve, reject) => {
  if (!fs.existsSync(`./eunomia.json`)) {
    reject("eunomia.json required within the root of the project")
  }
  else {
    const eunomia = system;

    if (!eunomia.generate.sitemap.enabled) {
      resolve()
      return false
    }

    if (isMissing(eunomia.generate.sitemap.routes)) {
      reject("The `eunomia.json` requires a `routes` json or string field")
    }
    else {
      if (!fs.existsSync(eunomia.generate.sitemap.routes)) {
        reject("file " + eunomia.generate.sitemap.routes + " does not exists")
      }
      else {
        try {
          const routes = JSON.parse(fs.readFileSync(eunomia.generate.sitemap.routes, `utf8`));
          const fullPath = `./node_modules/eunomia-controller/generate/sitemap/sitemap.ejs`
          const template = fs.readFileSync(fullPath, `utf8`);
          const render = ejs.compile(template, { filename: fullPath });
          const urls = routes
            .filter(url => !url.path.includes(`:`))
            .map(url => ({
              loc: url.path
            }));
          const output = render({config: eunomia.generate.sitemap, urls});
          fs.writeFileSync(`${writeToPath}/sitemap.xml`, output, `utf8`);
          resolve()
        }
        catch (e) {
          console.log(e)
          reject("Failed to create a sitemap.txt")
        }
      }
    }
  }
});