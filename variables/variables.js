const fs = require('fs');

// Contains all the regex for the parsing
const regex = {
  global: {
    //_: () => new RegExp("(@GLOBAL\\[(.*)?(?:\\s+)?,(?:\\s+)?([^\\s]{1,}?)(?:\\s+)?\\])", "ig"),
    _: () => new RegExp("(@GLOBAL\\[(?:\s+)?(.*?)\\,(?:\\s+)?(.*?)\\])", "ig"),
    contains: (body) => (typeof body === 'boolean' ? false : body.split(regex.global._()).length > 1)
  },
  script: {
    _: () => new RegExp("(\\$\\{((?:(?!\\$)[^}])+)\\}(?:\\((.{1,}?)\\))(?:\\:([Int|Number|String|Str|Boolean|Bool|Float|Long|Double]+))?)", "ig"),
    contains: (body) => (typeof body === 'boolean' ? false : body.split(regex.script._()).length > 1)
  },
  link: {
    _: () => new RegExp("\\#\\{(?:\\s+)?([^#}]+)(?:\\s+)?\\}", "ig"),
    contains: (body) => (typeof body === 'boolean' ? false : body.split(regex.link._()).length > 1)
  },
  file: {
    _: () => new RegExp("\\@\\{([\\w\\S\\s\\d]+?)\\}(?:\\(([\\w\\S\\s\\d]+?)\\))?(\\:(Image|Plain|Json))", "ig"),
    contains: (body) => (typeof body === 'boolean' ? false : body.split(regex.file._()).length > 1)
  },
  conversion: {
    _: () => new RegExp("\\((.*)(?:\\s+)?(?:\\s+)?\\)(?:\\s+)?~>(?:\\s+)?(Int|Number|String|Str|Boolean|Bool|Float|Long|Double)", "ig"),
    contains: (body) => (typeof body === 'boolean' ? false : body.split(regex.conversion._()).length > 1)
  }
};


// Contains the main functionality that uses the regex parsing
const compute = {
  global: (value, stack, originalConfig, internal, internalStack = 0) => {
    internalStack = internalStack + 1
    stack = stack + 1;
    if (internalStack <= 10) {
      if (regex.global.contains(value)){
        var pieces = value.split(regex.global._());
        const original = pieces[1]
        const global = pieces[2]
        const defaultResult = pieces[3]
        if(!(process.argv.join(" ").indexOf("co-mocha") > -1)) {
          if (new RegExp(`-{1,2}?${global}(?:\\s+)?=(?:\\s+)?([^\\s]+)?`, "ig").test(process.argv.join(" "))) {
            const argvPieces = new RegExp(`-{1,2}?${global}(?:\\s+)?=(?:\\s+)?([^\\s]+)?`, "ig").exec(process.argv.join(" "))
            const argvValue = argvPieces[1]
            return compute.global(value.replace(original, argvValue), stack, originalConfig, internal)
          }
          else if (new RegExp(`-{1,2}?${global}(?:[^\\s\\-]+)?`, "ig").test(process.argv.join(" "))) {
            return compute.global(value.replace(original, "false~>Boolean"), stack, originalConfig, internal)
          }
          else if (typeof process !== 'undefined' && typeof process.env !== 'undefined' && typeof process.env[global] !== 'undefined') {
            return compute.global(value.replace(original, process.env[global]), stack, originalConfig, internal)
          }
          else {
            return compute.global(value.replace(original, defaultResult), stack, originalConfig, internal)
          }
        }
        else if (regex.global.contains(value) && typeof process !== 'undefined' && typeof process.env !== 'undefined' && typeof process.env[global] !== 'undefined') {
          return compute.global(value.replace(original, process.env[global]), stack, originalConfig, internal)
        }
        else if (regex.global.contains(value)) {
          return compute.global(value.replace(original, defaultResult), stack, originalConfig, internal)
        }
        else {
          return value;
        }
      }
      else {
        return value;
      }
    }
    else {
      return "Stack Overflow"
    }
  },

  javascript: (value, stack, originalConfig, internal, internalStack = 0) => {
    internalStack = internalStack + 1
    stack = stack + 1;
    if (internalStack <= 10) {
      if (regex.script.contains(value)){
        try {
          var pieces = value.split(regex.script._());
          var script = pieces[2];
          var result = eval(script);
          var type = value.split(regex.script._())[4];
          return compute.javascript (value.replace(pieces[1], result), stack, originalConfig, internal);
        }
        catch(err) {
          return compute.javascript (value.replace(value.split(regex.script._())[1], value.split(regex.script._())[3]), stack, originalConfig, internal);
        }
      }
      else return value;
    }
    else {
      return "Stack Overflow"
    }
  },

  link: (value, stack, originalConfig, internal, internalStack = 0) => {
    internalStack = internalStack + 1
    stack = stack + 1;
    if (internalStack <= 10) {
      if (regex.link.contains(value)) {
        var result = value.replace(regex.link._().exec(value)[0], regex.link._().exec(value)[1].split('.').reduce((obj,i) => obj[i], originalConfig, internal));
        return internal(result, stack)
      }
      else return value;
    }
    else {
      return "Stack Overflow"
    }
  },

  file: (value, stack, originalConfig, internal, internalStack = 0) => value,

  conversion: (value, stack, originalConfig, internal, internalStack = 0) => {
    internalStack = internalStack + 1
    stack = stack + 1;
    if (internalStack <= 10) {
      var pieces = value.split(regex.conversion._());
      var left = pieces[1];
      var type = pieces[2];
      if (typeof pieces[0] !== 'undefined' && pieces[0] !== '' && typeof pieces[3] !== 'undefined' && pieces[3] !== '') {
        return pieces[0] + stringTypeConvert(type, left) + pieces[3];
      }
      else if (typeof pieces[3] !== 'undefined' && pieces[3] !== '') {
        return stringTypeConvert(type, left) + pieces[3];
      }
      else if (typeof pieces[0] !== 'undefined' && pieces[0] !== '') {
        return pieces[0] + stringTypeConvert(type, left);
      }
      else {
        return stringTypeConvert(type, left);
      }

    }
    else {
      return "Stack Overflow"
    }
  }
};

const generateConfig = (arr, originalConfig) => {
  const internal = (value, stack) => {
    stack = stack + 1;
    if (typeof value === 'number') return value;
    //if (typeof value === 'string') value = ((/(__([A-Z]+)__)/ig.test(value)) ? value.replace(/(__([A-Z]+)__)/ig, processGlobalVariable(value)) : value)
    if (stack <= 50) {
      if (regex.global.contains(value)) return internal(compute.global(value, stack, originalConfig, internal), stack);
      else if (regex.link.contains(value)) return internal(compute.link(value, stack, originalConfig, internal), stack);
      else if (regex.conversion.contains(value)) return internal(compute.conversion(value, stack, originalConfig, internal), stack);
      else if (regex.script.contains(value)) return internal(compute.javascript(value, stack, originalConfig, internal), stack);
      return value;
    }
    else return "Stack overflow"
  };

  // Loop through the JSON file mapping and computing the json file
  var newArray = {};
  Object.keys(arr).map(function(key, index) {
    if (typeof arr[key] === 'object' && typeof arr[key].length == 'undefined') newArray[key] = generateConfig(arr[key], originalConfig);
    else if(typeof arr[key] === 'object' && typeof arr[key].length !== 'undefined') {
      newArray[key] = arr[key].map((conf) => {
        if (typeof conf === 'object') return generateConfig(conf, originalConfig);
        else return internal(conf, 0);
      });
    }
    else {
      if (typeof arr[key] === 'string') newArray[key] = internal(arr[key], 0);
      else newArray[key] = arr[key];
    }
  });
  return newArray;
};

const stringTypeConvert = (type, value) => {
  if (typeof value === 'undefined') return String("");
  else if (type.toLowerCase() == "number" || type.toLowerCase() == "int" || type.toLowerCase() == "float" || type.toLowerCase() == "double") return Number(parseInt(value));
  else if (type.toLowerCase() == "string" || type.toLowerCase() == "str") return String(value);
  else if (type.toLowerCase() == "boolean" || type.toLowerCase() == "bool") {
    return value == true || value == 'true' || value == 1
  }
  else return String(value);
};

exports.generate = (configPath) => {
  if (!fs.existsSync(configPath)) {
    console.log('File ' + configPath + ' does not exist')
  }
  else {
    const systemConfig = JSON.parse(fs.readFileSync(configPath, 'utf8'));
    return generateConfig(systemConfig, systemConfig);
  }
}

