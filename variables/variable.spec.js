const chai = require('chai');
const expect = chai.expect;
const common = require('./index.js')("./variables/variable.spec.json")

describe('variable.js', function() {
  it('should be able to load a json file', function() {
    expect(common).to.not.equal(undefined);
  });
  it(`a === number`, function() {
    expect(common.a).to.be.a('number');
  });
  it(`b === string`, function() {
    expect(common.b).to.be.a('string');
  });
  it(`c === boolean`, function() {
    expect(common.c).to.be.a('boolean');
  });
  it(`d === number`, function() {
    expect(common.d).to.be.a('number');
  });
  it(`e === string`, function() {
    expect(common.e).to.be.a('string');
  });
  it(`f === number`, function() {
    expect(common.f).to.be.a('number');
  });
  it(`g === string`, function() {
    expect(common.g).to.be.a('string');
  });
  it(`h === broken calc`, function() {
    expect(common.h).to.equal("${10 + " + common.a + "}");
  });
  it(`i === string`, function() {
    expect(common.i).to.be.a('string');
  });
  it(`j === boolean`, function() {
    expect(common.j).to.be.a('boolean');
  });
  it(`k === string`, function() {
    expect(common.k).to.be.a('string');
  });
  it(`l === number`, function() {
    expect(common.l).to.be.a('number');
  });
  it(`m === number && === 30`, function() {
    expect(common.m).to.be.a('string');
    expect(common.m).to.equal('1000');
  });
});