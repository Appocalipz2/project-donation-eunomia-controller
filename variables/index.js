module.exports = (configPath) => require(`./variables`).generate(configPath);
/*
 {
 "a": 1,
 "b": "true",
 "c": true,
 "d": 0.00,
 "e": "${10 + 10}(0)",
 "f": "${10 + a}(0)~>Number",
 "g": "#{a}",
 "h": "${10 + #{a}}",
 "i": "${#{a} + #{a}}(0)",
 "j": "#{b}~>Boolean",
 "k": "#{b}~>String",
 "l": "@GLOBAL[GLOBAL_VARIABLE, 10]~>Number",
 "largeComputationsToDo": "@GLOBAL[GLOBAL_VARIABLE, 10]~>Number",
 "zz": "${${${@GLOBAL[GLOBAL_VARIABLE, 10]~>Number + @GLOBAL[TEST, 10]~>Number} * ${#{a}~>Number * #{a}~>Number}} * 10} ~> Number"
 }
 */