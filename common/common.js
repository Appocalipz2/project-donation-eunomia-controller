const fs = require(`fs`);
const ejs = require(`ejs`);
exports.execution = {
  isUndefined: (data, message) => typeof data === 'undefined'
}