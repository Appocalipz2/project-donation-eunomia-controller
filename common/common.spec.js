const chai = require('chai');
const expect = chai.expect;
const common = require('./index.js')

describe('common.js', function() {
  it('isUndefined() should return `true` on undefined values', function() {
    expect(common.isUndefined(undefined, "")).to.equal(true);
  });
  it('isUndefined() should return `false` on undefined values', function() {
    expect(common.isUndefined("defined", "")).to.equal(false);
  });
});